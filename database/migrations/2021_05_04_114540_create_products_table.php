<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedDouble('price');
            $table->text('description');
            $table->string('image_ids');
            $table->integer('amount');
            // type of product
            $table->unsignedBigInteger('type_id')->unsigned();
            $table->index('type_id');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
            // unit of product
            $table->unsignedBigInteger('unit_id')->unsigned();
            $table->index('unit_id');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            // category of product
            $table->unsignedBigInteger('category_id')->unsigned();
            $table->index('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            // user created
            $table->unsignedBigInteger('created_by')->unsigned();
            $table->index('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            // user deleted
            $table->unsignedBigInteger('deleted_by')->unsigned();
            $table->index('deleted_by');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
