<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItem extends Model
{

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carts';
}
