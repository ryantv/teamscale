<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function index(Request $request)
    {
        $data = $request->session()->all();
        dd($data);
    }
    
    /**
     * Store a product to cart.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     */
    public function store(Request $request, $id)
    {
        # validate

        // Check has product in storage
        $product = Product::find($id);
        if (empty($product)) {
            # handle when product not found ...
            dd('Product not found');
        }
        // check amount
        $amount = $request->amount;
        if ($product->amount < $request->amount) {
            # hanlde when amount of product not enough to add to cart
            $amount = $product->amount;
        }

        // put new cart item to session
        $data = [
            'product_id' => $id,
            'amount' => $amount,
            'price' => $product->price,
            'total' => $amount * $product->price
        ];
        $cart = $request->session()->get('cart', []);
        array_push($cart, $data);
        
        $request->session()->put(['cart' => $cart]);
        dd($request->session('cart'));
    }

    /**
     * Add a coupon to product in cart.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     */
    public function addCoupon(Request $request, $id)
    {
        # validate

        // Check has product in storage
        $product = Product::find($id);
        if (empty($product)) {
            # handle when product not found ...
            dd('Product not found');
        }
        // Check has coupon available
        $coupon = Coupon::where('code', $request->code)->first();
        if (empty($coupon)) {
            # handle when the coupon not found ...
            dd('Coupon not found');
        }
        if ($coupon->expired_date < Carbon::now()) {
            # handle when the coupon has expired ...
            dd('Coupon code has expired');
        }

        $cart = $request->session()->get('cart', []);
        // update cart item
        foreach ($cart as $key => &$cartItem) {
            if ($cartItem['product_id'] === $id) {
                $total = $cartItem['total'];
                switch ($coupon->type) {
                    case config('constants.coupon_type.amount'):
                        $cartItem['total'] = $total - $coupon->value;
                        break;
                    case config('constants.coupon_type.percent'):
                        $cartItem['total'] = ($total * $coupon->value) / 100;
                        break;
                }
                $cartItem['coupon'] = [
                    'value' => $coupon->value,
                    'type' => $coupon->type
                ];
            }
        }
        
        $request->session()->put('cart', $cart);
        dd($request->session('cart'));
    }

    /**
     * Remove the specified product from cart.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        // Check has product in storage
        $product = Product::find($id);
        if (empty($product)) {
            # handle when product not found ...
            dd('Product not found');
        }
        // handle delete cart item
        $cart = $request->session()->get('cart', []);
        foreach ($cart as $key => $cartItem) {
            if ($cartItem['product_id'] === $id) {
                unset($cart[$key]);
            }
        }
        if (count($cart)) {
            $request->session()->put('cart', $cart);
            dd(session('cart', []));
        } else {
            $request->session()->forget('cart');
            dd('Deleted Cart');
        }
    }
}
