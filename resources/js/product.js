export const products = [
    {
        id: 1,
        name: 'Nike 2019',
        description: 'Waterproof Shoes',
        categoryId: 'Nike',
        price: 200000,
        src: "/images/shoes.png",
        discount: {
            value: 20,
            type: 'Nike'
        },
        unit: {
            value: '',
            symbol: 'USD'
        }
    },
    {
        id: 2,
        name: 'Vans 2019',
        description: 'Giày chống nước',
        categoryId: 'Nike',
        price: 200000,
        src: "/images/shoes2.png",
        discount: {
            value: 30,
            type: 'Nike'
        },
        unit: {
            value: '',
            symbol: 'USD'
        }
    },
    {
        id: 3,
        name: 'AIR FORCE 1 OFF-WHITE',
        description: 'Giày chống nước',
        categoryId: 'Nike',
        price: 200000,
        src: "/images/shoes3.png",
        discount: {
            value: 50,
            type: 'Nike'
        },
        unit: {
            value: '',
            symbol: 'USD'
        }
    }
]
