import ProductsComponent from './components/ProductsComponent.vue';
import Cart from './components/Cart.vue';
import PageNotFound from './components/PageNotFound.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: ProductsComponent
    },
    {
        name: 'cart',
        path: '/cart',
        component: Cart
    },
    {
        name: 'page-not-found',
        path: '/*',
        component: PageNotFound
    }
];

